// console.log("hi")

/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication. (use return statement)

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division. (use return statement)

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided radius.
			-a number should be provided as an argument.
			-formula: area = pi * (radius ** 2)
			-exponent operator **
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation. (use return statement)

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

		Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-formula for average: num1 + num2 + num3 + ... divide by total numbers
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation. (use return statement) 

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed. (use return statement)
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/



	function letsAdd(num){
		let addition = num + 25;
		console.log("Display sum of 15 and 25:");
		console.log(num + 25);
	}	;

	letsAdd(15);

	function letsMinus(num1){
		let subtract = num1 - 20;
		console.log("Display differece of 40 and 20:");
		console.log(num1 - 20)
	};

	letsMinus(40);


	let  x = 10
	let y = 20
	function returnMultiply(x, y){
		let multiply = x * y
		console.log("The product of 10 and 20:")
		return multiply;
	}

	let solve = returnMultiply(x, y)
	console.log(solve);


	let  o = 30
	let t = 10
	function returnDivide(o, t){
		let divide = o / t
		console.log("The quotient of 30 and 10:");
		return divide;
	};

	let solve1 = returnDivide(o, t);
	console.log(solve1);

	function circleArea(pi, radius){
        let area = pi * (radius ** 2);
        console.log("The area of a circle with radius of 50 is?");
        return area
    };
    let area = circleArea("3.1416", "20");
    console.log(area);

    function fourNumbers(num1, num2 ,num3, num4){
    	let average = (10 + 50 + 100 + 200) / 4
    	console.log("The average of 20, 40, 60 and 80:");
    	return average
    };

    let average = fourNumbers();
    console.log(average);


  	function passingPercentage(average, average1) {
  	    let totalAverage = average * 0.75;
  	    let isPassed = average >= totalAverage;
  	    console.log('Is 90/100 a passing score?');
  	    return isPassed;
  	}

  	let passingScore = passingPercentage(90, 100);
  	console.log(passingScore);